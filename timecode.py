#!/usr/local/bin/python

import argparse


"""Usage notes
- Save sorted list to file and get feedback on command line:
$ python timecode.py timecode.txt timecode-sorted.txt
-- where 'timecode.txt' is the input file and 'timecode-sorted.txt' is the
    output file.  The output file is an optional, positional argument.

- Get feedback on command line without saving sorted list:
$ python timecode.py timecode.txt

- To get any particular type of feedback, use a flag:
$ python timecode.py timecode.txt -c
$ python timecode.py timecode.txt -d
$ python timecode.py timecode.txt -e

- Or combinations of flags:
$ python timecode.py timecode.txt -c -e
$ python timecode.py timecode.txt -d -c

- Try the verbose option in combination with -e:
$ python timecode.py timecode.txt -e -v

- Specify different entries to compare for elapsed time:
$ python timecode.py timecode.txt -e -o 1 1024 -v
"""


class Timecode(object):

    @staticmethod
    def create_parser():
        parser = argparse.ArgumentParser(
            description='Timecode processing options.')

        parser.add_argument('read_file_name', type=str, help="""
                            Required. Enter the path to the timecode file
                            that you would like to sort or analyze.  The file
                            should be a plaintext file with each timecode on
                            its own line.
                            """)
        parser.add_argument('write_file_name', nargs='?', type=str, help="""
                            Optional. Enter the path to which you would
                            like to save the sorted timecode file.
                            """)
        parser.add_argument('-c', '--count_per_hour', action='store_true',
                            help="""Optional. Return results of the
                            count_timecodes_per_hour() method.
                            """)
        parser.add_argument('-d', '--find_duplicates', action='store_true',
                            help="""Optional. Return results of the
                            find_duplicate_timecodes() method.
                            """)
        parser.add_argument('-e', '--elapsed_time', action='store_true',
                            help="""
                            Optional. By default, this method will calculate
                            the elapsed time between the 1st and 50th entries
                            in the sorted timecode list. Optionally use the -o
                            (--override) flag to override the defaults.
                            """
                            )
        parser.add_argument('-o', '--override', nargs=2, type=int,
                            help="""Optional. Use this flag to change the
                            default starting (1) and ending (50) entries for
                            calculating elapsed time. The first number
                            entered will be taken to be the starting entry,
                            and the second number will be taken to be the
                            ending entry. Enter exactly two, space-separated
                            natural numbers (i.e., enter 1 for the first
                            entry, not 0).
                            """)
        parser.add_argument('-v', '--verbose', action='store_true', help="""
                            Optional. Use this flag to display the start and
                            end timecodes used in the elapsed_time method (in
                            addition to the elapsed time).
                            """)

        return parser

    def process_request(self, args):
        """Determines which options are executed based on command line
        arguments.
        """
        """The default behavior of the program is to return duplicate
        timecodes, timecodes per hour, and elapsed time between two
        timecodes. If the user specifies one of those options explicitly,
        then only that and other explicitly specified options will be excecuted.
        In addition, if the user does not specify an output file, then the
        program procedes with only the aforementioned options.
        """
        timecodes = self.load_timecodes_from_file(args.read_file_name)
        sorted_timecodes = self.sort_timecodes(timecodes)
        """If args.write_file_name is None, then nothing happens. This allows
        the user to interact with the application without having to save a
        sorted file or even see it's output.
        """
        if args.write_file_name is not None:
            self.write_timecodes(args.write_file_name, sorted_timecodes)
        else:
            pass
        """Results are added to the data dictionary based on command line
        arguments.
        """
        data = {}
        if args.find_duplicates:
            duplicate_timecodes = self.find_duplicate_timecodes(
                sorted_timecodes)
            data['duplicate_timecodes'] = duplicate_timecodes
        else:
            pass
        if args.count_per_hour:
            timecodes_per_hour = self.count_timecodes_per_hour(sorted_timecodes)
            data['timecodes_per_hour'] = timecodes_per_hour
        else:
            pass
        if args.elapsed_time:
            elapsed_time = self.elapsed_time(sorted_timecodes, args)
            data['elapsed_time'] = elapsed_time
        else:
            pass
        """If no options are called out explicitly, then retrieve all of
        them. The --verbose option still works.
        """
        if not args.count_per_hour and not args.elapsed_time and \
                not args.find_duplicates:
            duplicate_timecodes = self.find_duplicate_timecodes(
                sorted_timecodes)
            timecodes_per_hour = self.count_timecodes_per_hour(sorted_timecodes)
            elapsed_time = self.elapsed_time(sorted_timecodes, args)
            data['duplicate_timecodes'] = duplicate_timecodes
            data['timecodes_per_hour'] = timecodes_per_hour
            data['elapsed_time'] = elapsed_time
        return self.format_response(data)

    @staticmethod
    def format_response(data):
        """Formats the command line output for human readability."""
        response = ''
        if 'duplicate_timecodes' in data:
            response += '==============================\n'
            response += "Duplicate Timecodes\n"
            for duplicate in data['duplicate_timecodes']:
                response += "\t%s\n" % duplicate
        else:
            pass
        if 'timecodes_per_hour' in data:
            response += '==============================\n'
            response += "Timecodes Per Hour\n"
            response += "      hour: count\n"
            top_key = 'timecodes_per_hour'
            for key in sorted(data[top_key].keys()):
                response += "\t%s: %s\n" % (key, data[top_key][key])
        else:
            pass
        if 'elapsed_time' in data:
            response += '==============================\n'
            response += "Elapsed Time\n"
            top_key = 'elapsed_time'
            """The try statement will work for a dictionary, and the except
            statement for a list.
            """
            try:
                for key in sorted(data[top_key].keys()):
                    response += "\t%s: %s\n" % (key, data[top_key][key])
            except AttributeError:
                for time in data['elapsed_time']:
                    response += "\t%s\n" % time
        else:
            pass
        return response

    @staticmethod
    def load_timecodes_from_file(filename):
        """Opens file specified from user input and reads in the timecodes."""
        timecodes = []
        with open(filename) as timecode_file:
            for timecode in timecode_file:
                timecodes.append(timecode)
        return timecodes

    @staticmethod
    def write_timecodes(filename, timecodes_to_write):
        """Writes sorted timecodes to a new file with a name specified by the
        user.
        """
        timecode_file = open(filename, 'w')
        for timecode in timecodes_to_write:
            timecode_file.write(timecode+"\n")
        timecode_file.close()

    @staticmethod
    def read_timecodes(filename):
        """Not currently used in this script, but used in testing."""
        timecode_file = open(filename, 'r')
        read_timecodes = timecode_file.read()
        return read_timecodes

    @staticmethod
    def count_timecodes(timecodes):
        """Counts the length of the list. Used to check that timecodes aren't
        dropped.
        """
        return len(timecodes)

    @staticmethod
    def convert_timecodes_to_frames(timecodes):
        """Converts timecodes to frames. Unsorted timecodes are passed to
        this method from sort_timecodes().
        """
        frames = []
        for timecode in timecodes:
            split = timecode.split(':')
            seconds = (int(split[0])*3600) + (int(split[1])*60) + \
                      (int(split[2]))
            frame = int((seconds * 24)) + int(split[3])
            frames.append(frame)
        return frames

    @staticmethod
    def convert_frames_to_timecodes(frames):
        """Converts frames to timecodes. Frames are passed from
        sort_timecodes() after they have been sorted.
        """
        timecodes = []
        for frame in frames:
            s, f = divmod(frame, 24)
            m, s = divmod(s, 60)
            h, m = divmod(m, 60)
            timecode = "%02d:%02d:%02d:%02d" % (h, m, s, f)
            timecodes.append(timecode)
        return timecodes

    @staticmethod
    def sort_frames(frames):
        """Sorts frames in ascending order. Frames are passed from
        sort_timecodes() after timecodes are converted to frames.
        """
        frames.sort()
        return frames

    def sort_timecodes(self, timecodes):
        """Mediates the methods that convert timecodes to frames, sort the
        frames, and convert the frames back to timecodes.
        """
        frames = self.convert_timecodes_to_frames(timecodes)
        sorted_frames = self.sort_frames(frames)
        sorted_timecodes = self.convert_frames_to_timecodes(sorted_frames)
        return sorted_timecodes

    @staticmethod
    def find_duplicate_timecodes(timecodes):
        """Compares each timecode in the sorted list to the next timecode in
        the list. The '-1' in the range prevents the loop from trying to
        compare the last element to a non-existent next element."""
        duplicate_timecodes = []
        for i in range(len(timecodes)-1):
            if timecodes[i] == timecodes[i+1]:
                duplicate_timecodes.append(timecodes[i])
                # duplicate_timecodes.append(timecodes[i+1])
        return duplicate_timecodes

    @staticmethod
    def count_timecodes_per_hour(timecodes):
        """Extracts the first two characters from each timecode, adds them to a
        dictionary, and iterates a count of how many times each occurs in the
        list. Does not require a sorted list.
        """
        timecodes_per_hour = {}
        for timecode in timecodes:
            hour = timecode[:2]
            if hour in timecodes_per_hour:
                timecodes_per_hour[hour] += 1
            else:
                timecodes_per_hour[hour] = 1
        return timecodes_per_hour

    @staticmethod
    def sum_hour_totals(hour_counts):
        """Sums the total number of counts for all hours. Used for testing to
        check that the sum equals the number of timecodes.
        """
        hour_total = 0
        for hour in hour_counts:
            count = hour_counts.get(hour)
            hour_total += count
        return hour_total

    def elapsed_time(self, timecodes, args):
        """Calculates elapsed time between any two timecodes in a list."""
        if args.override:
            start_timecode = timecodes[args.override[0]-1]
            end_timecode = timecodes[args.override[1]-1]
        else:
            start_timecode = timecodes[0]
            end_timecode = timecodes[49]
        timecodes_to_convert = [start_timecode, end_timecode]
        frames = self.convert_timecodes_to_frames(timecodes_to_convert)
        """The usage of abs() prevents negative frame_diffs, and ultimately
        makes the order in which the user enters the references irrelevant.
        """
        frame_diff = [abs(frames[1] - frames[0])]
        timecode = self.convert_frames_to_timecodes(frame_diff)
        if args.verbose:
            response = {'start_timecode': start_timecode, 'end_timecode':
                        end_timecode, 'elapsed_time': timecode[0]}
            return response
        else:
            return timecode


def sort_timecodes():
    time_code = Timecode()
    parser = time_code.create_parser()
    args = parser.parse_args()
    processed_timecodes = time_code.process_request(args)
    print processed_timecodes


if __name__ == '__main__':
    sort_timecodes()
