# The problem
Write a program that will parse a text file containing 24FPS video timecodes, sort the timecode entries in ascending order, and output the sorted timecodes to a new text file that is specified by the user.  See [Timecode and Frame Rates: Everything You Need to Know](https://blog.frame.io/2017/07/17/timecode-and-frame-rates/) for helpful background information.

Additional required functionality:
* list all occurrences of duplicate timecode entries in the list
* for each hour of time represented by the entries, give a total count of
  timecode entries that occur within that hour
* take the first entry and the fiftieth entry in the sorted list, and print the
  elapsed time between the two (as timecode) 

# Usage notes
* Save sorted list to file and get feedback on command line:  
  `timecode.py input_file output_file`  
  * `input_file` is a required, positional argument
  * `output_file` is an optional, positional argument; if no `output_file` is
    specified, the program will neither display nor save the sorted list
  * by default, duplicate timecodes, counts per hour, and elapsed time between 
    specified entries will be displayed unless overridden by flags
  * Example usage: 
    `$ timecode.py timecode.txt timecode-sorted.txt`

* To get any particular type of feedback, use a flag:  
  `timecode.py input_file flag`  
  * `flag` can be any combination of the following:
    * `-c` or `--count_per_hour`
    * `-d` or `--find_duplicates`
    * `-e` or `--elapsed_time` by default, returns elapsed time between 1st and
      50th entries; can be overridden with `-o` flag
  * Example usage:
    `$ timecode.py timecode.txt -c`
    `$ timecode.py timecode.txt -d`
    `$ timecode.py timecode.txt -e`

    combinations of flags:
    `$ timecode.py timecode.txt -c -e`
    `$ timecode.py timecode.txt -d -c`

* Try the verbose option in combination with -e:  
  `timecode.py timecode.txt -e flag`  
  * where `flag` is `-v` or `--verbose` displays the start and end timecodes 
    used to calculate elapsed time
  * `$ timecode.py timecode.txt -e -v`

* Specify different entries to compare for elapsed time:  
  `timecode.py input_file -e `flag` first_number second_number`  
  * where `flag` is `-o` or `--override` (override default setting of 1 and 50)
  * `first_number` is the first timecode used in the elapsed time calculation;
    required with `-o`
  * `second_number` is the last timecode used in the elapsed time calculation;
    required with `-o`
  * Example usage:
    `$ python timecode.py timecode.txt -e -o 1 1024 -v`

# Unit tests
Unit tests are available in timecode_tests.py

