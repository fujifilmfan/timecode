#!/usr/local/bin/python
import unittest
import os
from timecode import Timecode


class TimecodeTestCase(unittest.TestCase):
    # sets up a parser and Timecode instance
    @classmethod
    def setUpClass(cls):
        cls.time_code = Timecode()
        cls.parser = cls.time_code.create_parser()

    @staticmethod
    def get_timecode():
        timecode = ['23:02:38:19']
        return timecode

    @staticmethod
    def get_unsorted_timecodes():
        timecodes = [
            '19:20:22:13',
            '10:13:47:03',
            '11:29:41:06',
            '11:59:40:07',
            '16:21:45:22',
            '01:13:42:15',
            '06:28:22:19',
            '06:52:40:21',
            '21:52:48:02',
            '21:52:48:02',
            '16:21:45:22'
        ]
        return timecodes

    @staticmethod
    def get_sorted_timecodes():
        timecodes = [
            '01:13:42:15',
            '06:28:22:19',
            '06:52:40:21',
            '10:13:47:03',
            '11:29:41:06',
            '11:59:40:07',
            '16:21:45:22',
            '16:21:45:22',
            '19:20:22:13',
            '21:52:48:02',
            '21:52:48:02'
        ]
        return timecodes

    @staticmethod
    def get_duplicate_timecodes():
        duplicates = [
            '16:21:45:22',
            '21:52:48:02'
        ]
        return duplicates

    @staticmethod
    def get_timecodes_per_hour():
        timecodes_per_hour = {
            '01': 1,
            '06': 2,
            '10': 1,
            '11': 2,
            '16': 2,
            '19': 1,
            '21': 2
        }
        return timecodes_per_hour

    @staticmethod
    def get_frame():
        frame = [1991011]
        return frame

    @staticmethod
    def get_unsorted_frames():
        frames = [
            1670941,
            883851,
            993150,
            1036327,
            1413742,
            106143,
            559267,
            594261,
            1890434,
            1890434,
            1413742
        ]
        return frames

    @staticmethod
    def get_sorted_numbers():
        frames = [
            106143,
            559267,
            594261,
            883851,
            993150,
            1036327,
            1413742,
            1413742,
            1670941,
            1890434,
            1890434
        ]
        return frames

    @staticmethod
    def get_elapsed_time():
        # frames: 1784291
        elapsed_time = ['20:39:05:11']
        return elapsed_time

    @staticmethod
    def read_file_name():
        filename = "/Users/acetone/Dropbox/code/PycharmProjects/" \
                   "timecode/timecode.txt"
        return filename

    @staticmethod
    def write_file_name():
        filename = "/Users/acetone/Dropbox/code/PycharmProjects/" \
                   "timecode/timecode_test-sorted.txt"
        return filename

    def get_file_contents(self):
        timecodes = []
        with open(self.read_file_name()) as timecode_file:
            for timecode in timecode_file:
                timecodes.append(timecode)
        return timecodes

    @staticmethod
    def get_file_text():
        text_to_write = "This line"
        return text_to_write

    @staticmethod
    def get_formatted_file_text():
        formatted_text = ""
        text_to_write = "This line"
        for character in text_to_write:
            formatted_text += (character+"\n")
        return formatted_text

    # File operations tests
    def test_load_timecodes(self):
        timecodes = Timecode().load_timecodes_from_file(self.read_file_name())
        self.assertEqual(timecodes, self.get_file_contents())

    def test_write_file(self):
        Timecode().write_timecodes(self.write_file_name(), self.get_file_text())
        self.assertTrue(os.path.isfile(self.write_file_name()))

    def test_read_file(self):
        timecode_file = Timecode()
        self.assertEqual(timecode_file.read_timecodes(self.write_file_name()),
                         self.get_formatted_file_text())

    # Conversion tests
    def test_convert_timecode_to_frame(self):
        self.assertEqual(self.time_code.convert_timecodes_to_frames(
            self.get_timecode()), self.get_frame())

    def test_convert_frame_to_timecode(self):
        self.assertEqual(self.time_code.convert_frames_to_timecodes(
            self.get_frame()), self.get_timecode())

    def test_convert_timecode_to_frame_to_timecode(self):
        frame = self.time_code.convert_timecodes_to_frames(self.get_timecode())
        timecode = self.time_code.convert_frames_to_timecodes(frame)
        self.assertEqual(timecode, self.get_timecode())

    def test_convert_frame_to_timecode_to_frame(self):
        timecode = self.time_code.convert_frames_to_timecodes(self.get_frame())
        frame = self.time_code.convert_timecodes_to_frames(timecode)
        self.assertEqual(frame, self.get_frame())

    def test_convert_timecodes_to_frames(self):
        self.assertEqual(self.time_code.convert_timecodes_to_frames(
            self.get_unsorted_timecodes()), self.get_unsorted_frames())

    def test_convert_frames_to_timecodes(self):
        self.assertEqual(self.time_code.convert_frames_to_timecodes(
            self.get_unsorted_frames()), self.get_unsorted_timecodes())

    def test_convert_timecodes_to_frames_to_timecodes(self):
        frames = self.time_code.convert_timecodes_to_frames(
            self.get_unsorted_timecodes())
        timecodes = self.time_code.convert_frames_to_timecodes(frames)
        self.assertEqual(timecodes, self.get_unsorted_timecodes())

    def test_convert_frames_to_timecodes_to_frames(self):
        timecodes = self.time_code.convert_frames_to_timecodes(
            self.get_unsorted_frames())
        frames = self.time_code.convert_timecodes_to_frames(timecodes)
        self.assertEqual(frames, self.get_unsorted_frames())

    # Sort tests
    def test_sort_frames_without_conversion(self):
        frames = self.time_code.sort_frames(self.get_unsorted_frames())
        self.assertEqual(frames, self.get_sorted_numbers())

    def test_sort_timecodes_without_conversion(self):
        timecodes = self.time_code.sort_timecodes(self.get_unsorted_timecodes())
        self.assertEqual(timecodes, self.get_sorted_timecodes())

    # Convert and sort test
    def test_convert_and_sort_timecodes(self):
        frames = self.time_code.convert_timecodes_to_frames(
            self.get_unsorted_timecodes())
        sorted_frames = self.time_code.sort_frames(frames)
        timecodes = self.time_code.convert_frames_to_timecodes(sorted_frames)
        self.assertEqual(timecodes, self.get_sorted_timecodes())

    # Feedback tests
    def test_duplicate_timecode_entries(self):
        duplicates = self.time_code.find_duplicate_timecodes(
            self.get_sorted_timecodes())
        self.assertEqual(duplicates, self.get_duplicate_timecodes())

    def test_total_timecodes_per_hour(self):
        timecodes_per_hour = self.time_code.count_timecodes_per_hour(
            self.get_sorted_timecodes())
        self.assertEqual(timecodes_per_hour, self.get_timecodes_per_hour())

    def test_elapsed_time_between_timecodes(self):
        args = self.parser.parse_args(['read_file_name', '--override', '1',
                                       '11'])
        elapsed_time = self.time_code.elapsed_time(
            self.get_sorted_timecodes(), args)
        self.assertEqual(elapsed_time, self.get_elapsed_time())

    def test_count_timecodes(self):
        count = self.time_code.count_timecodes(self.get_sorted_timecodes())
        self.assertEqual(count, len(self.get_sorted_timecodes()))

    def test_sum_hour_totals(self):
        hour_total = self.time_code.sum_hour_totals(
            self.get_timecodes_per_hour())
        self.assertEqual(hour_total, len(self.get_sorted_timecodes()))

suite = unittest.TestLoader().loadTestsFromTestCase(TimecodeTestCase)
unittest.TextTestRunner(verbosity=2).run(suite)
